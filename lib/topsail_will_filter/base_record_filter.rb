module TopsailWillFilter
  class BaseRecordFilter
    #
    # matchers
    #
    def filter_records(rel = filterable_type.constantize)
      check_filterable_type!(rel)
      rel.filter_by(to_hash)
    end

    def self.matching(_record)
      raise 'TBD'
    end

    #
    # factory
    #
    def clone
      self.class.from_filter(self)
    end

    def self.from_filter(another_filter)
      new(filterable_type: another_filter.filterable_type, conditions: another_filter.to_hash)
    end

    #
    # accessors
    #
    def [](key)
      to_hash[key]
    end

    def to_hash
      raise 'Implemented By Subclass'
    end

    def filterable_type
      raise 'Implemented By Subclass'
    end

    protected

    def check_filterable_type!(rel)
      return if rel <= filterable_type.constantize

      raise "Conflicting Filterable Type (#{rel.sti_name} != #{filterable_type})"
    end
  end
end
