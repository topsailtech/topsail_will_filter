module TopsailWillFilter
  class RecordFilterCondition < ActiveRecord::Base
    belongs_to :record_filter, inverse_of: :record_filter_conditions

    validates :record_filter, presence: true
    validates :key, presence: true, uniqueness: {scope: :record_filter_id}
    validates :value, presence: true
  end
end
