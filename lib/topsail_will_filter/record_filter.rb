# not yet enabled; turn it on in topsail_will_filter.rb when ready!
module TopsailWillFilter
  class RecordFilter < BaseRecordFilter
    include ActiveRecord::Base

    has_many :record_filter_conditions, inverse_of: :record_filter

    validates :filterable_type, presence: true

    def conditions=(conditions_hash)
      record_filter_conditions.build(conditions_hash.map{ |k,v| {key:k, value: v}})
    end

    def to_hash
      record_filter_conditions.inject({}) { |run, c|
        run[c.key] = c.value
        run
      }
    end
  end
end
