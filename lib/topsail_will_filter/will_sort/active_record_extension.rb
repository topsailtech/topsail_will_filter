# ActiveRecord classes can call #will_sort(mapper), where mapper defines recipes how to sort relations.
#
# Afterwards, you can ask the AR/Relation for
#             sorted_by('sort_key')
#             sorted_by('sort_key desc')
#             sorted_by(['sortkey1', 'sortkey2', ...])
# The resulting relation will have an appropriate #order clause,
# and also answers to the new methods
#             sorted_by
#             sorted_by?(sort_key)
#             sort_index
#             sort_dir(sort_key),
#             sortable_by?(sort_key)
#             sortable_by
#
# mapper is a hash that maps a sort_key to a
#  a) string that can be used as an ORDER BY clause
#  b) a lambda scope (e.g.   ->{includes('owner').order('owner.name')}
#  The key :default has special meaning and will be used if no explicit sort_key was given.
#     The value of :default should be either another sort_key or string for the ORDER clause
# If a sort_key is not found in the mapper, it is passed through as the ORDER string as a default.

require 'topsail_will_filter/will_sort/relation_methods'
require 'topsail_will_filter/will_sort/current_sort'
require 'topsail_will_filter/will_sort/sorter'
require 'topsail_will_filter/will_sort/reversible_sql_literal'

module TopsailWillFilter
  module WillSort
    module ActiveRecordExtension
      extend ActiveSupport::Concern
      included do
      end

      module ClassMethods
        def will_sort(new_mapper = nil)
          @will_sort = new_mapper.symbolize_keys
        end

        def sorted_by(what = nil)
          # clone and get all the goodness from the rel_methods;
          # making sure we have a ActiveRecord_Relation
          rel = all.extending(TopsailWillFilter::WillSort::RelationMethods)
          raise 'sorted_by already called before' unless rel.instance_variable_get('@current_sort').nil?

          sort_strings = Array(what).map(&:to_s).reject(&:blank?) # create an array of sort strings; [] if what == nil
          if sort_strings.empty? && (default_sort = sort_recipes[:default])
            sort_strings << default_sort.to_s
          end

          rel.instance_variable_set('@current_sort', CurrentSort.new(sort_strings))

          Sorter.new(rel).apply! unless sort_strings.empty? # all the work happens here!
          rel
        end

        def sortable_by
          sort_recipes.keys - [:default]
        end

        def sortable_by?(sort_key)
          sortable_by.include?(sort_key.to_sym)
        end

        # Dive through the ancestors and find an over-all hash of sort recipes
        def sort_recipes
          ancestors.reverse.each_with_object({}) do |klass, answer|
            sorts_for_that_class = klass.instance_variable_get('@will_sort')
            answer.merge!(sorts_for_that_class) unless sorts_for_that_class.blank?
          end
        end
      end
    end

    class SortRecipeMissing < StandardError; end
    class SortRecipeNotAProc < StandardError; end
    class NotReversibleSort < StandardError; end
  end
end

ActiveRecord::Base.include TopsailWillFilter::WillSort::ActiveRecordExtension
