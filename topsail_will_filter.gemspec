# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'topsail_will_filter/version'

Gem::Specification.new do |spec|
  spec.name          = "topsail_will_filter"
  spec.version       = TopsailWillFilter::VERSION
  spec.authors       = ["Jan Schroeder"]
  spec.email         = ["jschroeder@topsailtech.com"]

  spec.summary       = "Helps filtering and sorting AR relations"
  spec.description   = "Also provides helpers like wrappers that lets filters be used in form_builders, and actual RecordFilter AR model"
  spec.homepage      = "http://www.topsailtech.com"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "bin"
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com' to prevent pushes to rubygems.org, or delete to allow pushes to any server."
  end

  spec.add_dependency "rails", "> 4.0.0"

  spec.add_development_dependency "bundler", "~> 1.8"
  spec.add_development_dependency "rake", "~> 10.0"
end
