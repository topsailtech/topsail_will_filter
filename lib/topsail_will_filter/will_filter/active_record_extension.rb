require 'topsail_will_filter/will_filter/relation_methods'
require 'topsail_will_filter/will_filter/current_filter'

module TopsailWillFilter
  module WillFilter
    module ActiveRecordExtension
      extend ActiveSupport::Concern

      included do
      end

      module ClassMethods
        def will_filter(new_mapper = nil)
          @will_filter = new_mapper.symbolize_keys
        end

        def filter_by(filters_hash = nil)
          all.extending(TopsailWillFilter::WillFilter::RelationMethods).
            _filter_by(filters_hash.is_a?(ActionController::Parameters) ? filters_hash.to_unsafe_h : filters_hash || {})
        end

        def filterable_by?(filter_key)
          filter_recipies.include?(filter_key.to_sym)
        end

        def filterable_by
          filter_recipies.keys
        end

        # Dive through the ancestors and find an over-all hash of filter recipies
        def filter_recipies
          answer = {}
          ancestors.reverse.each { |klass|
            filter_for_that_class = klass.instance_variable_get('@will_filter')
            answer = answer.merge(filter_for_that_class) unless filter_for_that_class.blank?
          }
          answer
        end
      end
    end
  end
end

ActiveRecord::Base.send :include, TopsailWillFilter::WillFilter::ActiveRecordExtension
