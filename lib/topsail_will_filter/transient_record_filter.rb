#
#  If it isn't stored in it's own model....
#
module TopsailWillFilter
  class TransientRecordFilter < BaseRecordFilter
    attr_accessor :conditions,
                  :filterable_type

    def initialize(filterable_type: , conditions: {})
      super
      self.filterable_type = filterable_type.to_s
      self.conditions = conditions.is_a?(ActionController::Parameters) ? conditions.to_unsafe_h : conditions
    end

    def merge!(more_conditions)
      @conditions.merge!(more_conditions)
    end

    def except!(*condition_keys)
      @conditions.except!(*condition_keys)
    end

    def to_hash
      @conditions.clone
    end
  end
end
