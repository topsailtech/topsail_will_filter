module TopsailWillFilter
  module WillSort
    # Wraps a SQL string, and has logic to reverse the sort order.
    #
    # Until Rails 6.1 Arel is not able to reverse order  statements where ASC/DESC
    # id not the last word.
    # E.g. Book.order(Arel.sql('rating NULLS LAST')).reverse_order throws
    #  => ActiveRecord::IrreversibleOrderError: Order "rating NULLS LAST" can not be reversed automatically
    #
    # ReversibleSqlLiteral lets you specify explicitly which SQL to use for ASC and for DESC ordering.
    #
    # will_sort latest_login:
    #             TopsailWillFilter::WillSort::ReversibleSqlLiteral.new(
    #               -> { order(Arel.sql('latest_login NULLS LAST')) },
    #               -> { order(Arel.sql('latest_login DESC NULLS LAST')) }
    #             )
    #           }
    class ReversibleSqlLiteral
      def initialize(asc, desc)
        @asc_rel = asc
        @desc_rel = desc
      end

      def asc
        @asc_rel
      end

      def desc
        @desc_rel
      end
    end
  end
end
