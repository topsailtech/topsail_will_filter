require 'topsail_will_filter/will_filter/filter'

module TopsailWillFilter
  module WillFilter
    class CurrentFilter
      def initialize(hash)
        @filters_hash = hash
      end

      def filter_keys
        @filters_hash ? @filters_hash.keys : []
      end

      def filter_hash
        @filters_hash ? @filters_hash.clone : {}
      end

      # get a nice object for the filter that can be used in form_for;
      # E.g. you can ask it .filter.start_date_gt
      def filter
        Filter.new(@filters_hash)
      end
    end
  end
end
