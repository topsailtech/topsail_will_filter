require "topsail_will_filter/version"

module TopsailWillFilter
  # Your code goes here...
  require 'topsail_will_filter/will_filter/active_record_extension'
  require 'topsail_will_filter/will_sort/active_record_extension'

  require 'topsail_will_filter/base_record_filter'
  require 'topsail_will_filter/transient_record_filter'
  # require 'topsail_will_filter/record_filter'
end
