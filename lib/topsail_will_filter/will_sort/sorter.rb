module TopsailWillFilter
  module WillSort
    class Sorter
      REVERSABLE_NODES = [
        Arel::Nodes::Ordering,
        Arel::Nodes::Function, # Rails can't reverse subclass NamedFunction, but we turn them into a reversable Nodes::Ordering
        Arel::Nodes::SqlLiteral # TODO: this needs more inspection. Not all SQL is auto-reversable
      ].freeze

      def initialize(rel)
        @rel = rel
      end

      def apply!
        sorted_by = @rel.sorted_by_hash
        sorted_by.each_with_index do |(sorted_by_value, sort_dir), idx|
          ordering_scope = scope_for_recipe(sorted_by_value, sort_dir)

          # we need to drop possible defined secondary sorts if not last sort key
          if idx < sorted_by.size - 1 && ordering_scope.order_values.size > 1
            ordering_scope.order_values = [ordering_scope.order_values.first]
          end

          @rel.merge!(ordering_scope)
        end
      end

      private

      # @param sort_key
      # @param direction - :asc or :desc
      def scope_for_recipe(sort_key, direction)
        sort_recipe = @rel.sort_recipes[sort_key]
        case sort_recipe
        when nil
          raise SortRecipeMissing, "No sort recipe found for key '#{sort_key}'"
        when ReversibleSqlLiteral
          sort_proc = direction == :asc ? sort_recipe.asc : sort_recipe.desc
          @rel.model.instance_exec(&sort_proc)
        when Proc
          recipe_result = @rel.model.instance_exec(&sort_recipe)
          raise NotReversibleSort, "Sort '#{sort_key}' must be reversible" unless reversible?(recipe_result)

          direction == :asc ? recipe_result : reverse_order(recipe_result)
        else
          raise SortRecipeNotAProc, "Value for key '#{sort_key}' must be a Proc"
        end
      end

      # Checking if we can really reverse order a rel. For this we whitelist order fragments
      # that either Rails reverses natively, or the Sorter can reverse.
      def reversible?(rel)
        rel.order_values.all? { |v| REVERSABLE_NODES.any? { |klass| v.is_a?(klass) } }
      end

      def reverse_order(ordered_rel)
        # NamedFunction (subclass of Function) are not reversing. Turning them into Nodes::Ordering
        ordered_rel.order_values.each_with_index do |val, idx|
          ordered_rel.order_values[idx] = val.asc if val.is_a?(Arel::Nodes::NamedFunction)
        end
        ordered_rel.reverse_order
      end
    end
  end
end
