module TopsailWillFilter
  module WillFilter
    module RelationMethods
      attr_writer :_current_filter

      delegate :filter_hash, :filter_keys, :filter, to: :_current_filter

      def _current_filter
        @_current_filter ||= CurrentFilter.new({})
      end

      # apply a filter. If we already did filter before, merge the remembered filters.
      def _filter_by(new_filters_hash)
        rel = self

        new_applicable_filters_hash =
          new_filters_hash.select{ |k,v| value_present?(v) }. # reject filters out right if the values are not really present for that filter
          symbolize_keys.
          # also reject filters if they return nil (gives filter procs the ability to reject values upon closer inspection)
          reject{ |k, v|
            filter_recipe = filter_recipies[k]
            filter_recipe.is_a?(Proc) && rel.instance_exec(v, &filter_recipe).nil?
          }

        #   when enabled, the folowing code breaks Farrins intakes filter when a custom filter is set and another filter (e.g. :specialty_names) conflicts....
        # conflicting_filter_key = new_applicable_filters_hash.keys & self.filter_keys
        # raise "This Relation was already filtered by #{conflicting_filter_key}" unless conflicting_filter_key.empty?

        @filters_hash = (@filters_hash || {}).merge(new_applicable_filters_hash)

        new_applicable_filters_hash.each { |k,v|
          filter_recipe = filter_recipies[k]
          if filter_recipe.is_a?(Proc)
            rel = rel.instance_exec(v, &filter_recipe)
          elsif filter_recipe.nil? # plain column name
            rel = rel.where(k => v)
          else
            rel = rel.where(filter_recipe => v) # some column name
          end
        }
        # somhow the extension gets lost, need to re-extend here
        rel = rel.extending(RelationMethods)
        rel._current_filter = CurrentFilter.new(@filters_hash)
        rel
      end

      private

      def value_present?(a_value)
        case a_value
        when nil, '' then false
        when Array then a_value.size != 1 || a_value[0].present?
        when Hash then a_value.values.detect { |v2| value_present?(v2) }
        else true
        end
      end
    end
  end
end
