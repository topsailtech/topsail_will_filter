require 'ostruct'

module TopsailWillFilter
  module WillFilter
    class Filter < OpenStruct
      include ActiveModel::AttributeMethods
      extend ActiveModel::Naming

      def initialize(hash)
        super
        hash.each{ |k, v|
          self[k] = Filter.new(v) if v.is_a?(Hash)
        }
      end

      def key?(key)
        to_h.key?(key.to_sym)
      end

      def [](key)
        send key.to_sym
      end
    end
  end
end
