module TopsailWillFilter
  module WillSort
    class CurrentSort
      # @param sorted_by_cfg [Hash|Array]- {sort_key_1: :asc, sort_key_2: :desc, ...} or
      #                                    ['sort_key_1', 'sort_key_2 desc', ...]
      def initialize(sorted_by_cfg)
        @sorted_by = if sorted_by_cfg.is_a?(Hash)
                       sorted_by_cfg
                     else
                       self.class.sort_hash_from_strings(sorted_by_cfg)
                     end
      end

      def sorted_by_hash
        @sorted_by&.clone || {}
      end

      def sort_index(sort_key)
        @sorted_by&.keys&.index(sort_key.to_sym)
      end

      def sorted_by?(sort_key)
        @sorted_by && sort_key && @sorted_by.keys.include?(sort_key.to_sym)
      end

      # @return nil, :asc, :desc
      def sort_dir(sort_key = nil)
        if @sorted_by.nil?
          nil
        elsif sort_key.nil? && @sorted_by.size == 1
          @sorted_by.values.first
        elsif sort_key.nil?
          nil
        else
          @sorted_by[sort_key.to_sym]
        end
      end

      def self.sort_hash_from_strings(sort_strings)
        ActiveSupport::OrderedHash[sort_strings.map do |s|
          parts = s.split
          [parts[0].to_sym, parts[1] && parts[1].downcase == 'desc' ? :desc : :asc]
        end]
      end
    end
  end
end
