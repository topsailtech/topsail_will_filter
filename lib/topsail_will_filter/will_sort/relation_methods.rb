module TopsailWillFilter
  module WillSort
    module RelationMethods
      delegate :sorted_by?, :sorted_by_hash, :sort_dir, :sort_index, to: :@current_sort

      def primary_sort_attr
        primary_order_attr = order_values.first.to_sql
        if primary_order_attr.downcase.ends_with?(' asc', ' desc')
          primary_order_attr.split(' ')[0..-2].join(' ')
        else
          primary_order_attr
        end
      end

      def primary_sort_type
        result_value = limit(1).reorder(nil).pluck(primary_sort_attr).first
        case result_value
        when NilClass then :nil
        when String   then :string
        when Date     then :date
        when Time     then :time
        when Numeric  then :numeric
        end
      end

      def rank_for_value(value)
        primary_sort_dir = sorted_by_hash.values.first
        where("#{primary_sort_attr} #{primary_sort_dir == :asc ? '<' : '>'} ?", value.downcase).count + 1
      end
    end
  end
end
